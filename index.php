<!DOCTYPE　html>
<html lang="ja">
<head>
  <meta charset="utf-8">

  <LINK rel="stylesheet" type="text/css" href="index_style.php">
  <title>カレンダー</title>
</head>
<body>

<div id='header'>
</div>
<?php

$week = array("月","火","水","木","金","Sat","Sun");

$ym = isset($_GET['ym']) ? $_GET['ym'] : date("Y");

list($yy, $mm) = explode('-', $ym);

//祝日取得

// カレンダーID
$calendar_id = urlencode('japanese__ja@holiday.calendar.google.com');

// 取得期間
$start  = date("$yy-01-01\T00:00:00\Z");
$end = date("$yy-12-31\T00:00:00\Z");

$url = "https://www.google.com/calendar/feeds/{$calendar_id}/public/basic?start-min={$start}&start-max={$end}&max-results=30&alt=json";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true) ;
$result = curl_exec($ch);
curl_close($ch);
$holiday_dates = array();

if (!empty($result)) {
  $json = json_decode($result, true);
  if ( !empty($json['feed']['entry'])){
    foreach($json['feed']['entry'] as $val){
      $date = preg_replace('#\A.*?(2\d{7})[^/]*\z#i','$1',$val['id']['$t']);
      array_push($holiday_dates, preg_replace('/\A(\d{4})(\d{2})(\d{2})/','$1-$2-$3', $date));
    }
  }
}



$prev =  date("Y-m", mktime(0,0,0,$mm,1,$yy-1));

$next =  date("Y-m", mktime(0,0,0,$mm,1,$yy+1));

$endday = date("d", mktime(0,0,0,$mm+1,0,$yy));


$mm = "00";


echo"<Center>";

      echo"<a class='prev' href='?ym=$prev' >".('&lt;')."</a>";
    echo"<font class='top_year'>".$yy."</font>";
      echo"<a class='next' href='?ym=$next'>".('&gt;')."</a>";

// table(外枠)


echo"<table class='outframe'>";

  for($i = 0;$i < 4;$i++){

    echo"<tr class='outframe'>";

  for($j = 0;$j < 3;$j++){

  $mm++;

  $endday = date("d", mktime(0,0,0,$mm+1,0,$yy));

  $first_week = date('w',strtotime("$yy/$mm/1"));

  if ($first_week == 0){
    $first_week = 7;
  }

  $day = 1;

echo"<td class='outframe'><table>";
  echo"<tr>";
    echo"<th class='ym' colspan='7'>".($mm)."</th>";

    echo"<th>";
 echo"</tr>";

//配列から曜日取得
    echo"<tr>\n";
    for($td = 0;$td < 7;$td++){
      echo"<td bgcolor='CornflowerBlue'><font color='white'>".($week[$td])."</font></td>\n";
    }
    echo"</tr>\n";

for($tr = 0;$tr < 6;$tr++){

  echo"<tr>\n";

  for($td = 0;$td < 7;$td++){

    if($first_week != 1){

//前月、次月の日にちは空白
      echo"<td>".('&nbsp;')."</td>\n";
      $first_week--;

    }else if($endday < $day){
        echo"<td>".('&nbsp;')."</td>\n";
      }else{
//日にち取得
        $dd = sprintf('%02d',$day);
        $MM = sprintf('%02d',$mm);
        $today = "$yy-$MM-$dd";

        //今日が祝日の場合
        if($today == date('Y-m-j') && in_array($today, $holiday_dates)){
          echo"<td
           id='holiday_today'class='week_$td' >".($day)."</td>\n";

           //今日に背景つける

        } else if($today == date('Y-m-j')){
          echo"<td id='today'class='week_$td' >".($day)."</td>\n";

           //祝日に赤

        }else if(in_array($today, $holiday_dates)){
      echo"<td
       id='holiday'class='week_$td'>".($day)."</td>\n";

      }else{
      echo"<td
      class='week_$td'>".($day)."</td>\n";
      }
      $day++;
          }
      }
  echo"</tr>\n";

}
echo"</table></td>\n";
}
}
echo"</tr>";
echo"</table>";
echo"</Center>";

echo"<a href='#header' id='yearfixed'>".($yy)."</p>";
?>

</body>
</html>
